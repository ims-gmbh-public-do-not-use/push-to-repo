# Push To Repo

Commit changes back to the repo from a GitLab CI job.

Uses GitLab API and by default fails quietly (if the pushed file is for example unchanged). Easy to use and flexible to configure.

## Configuration

1. Add `GL_PRIVATE_TOKEN` to your GitLab repo CI/CD variables with a valid GitLab access token as its value.

## Usage

`npx push-to-repo -h`

```
Usage: push-to-repo -f <file> [options]

Options:
  -f, --file-name <filename>  the file to push
  -b, --branch <branch>       the branch to push (default: "CI_COMMIT_BRANCH")
  -m, --message <message>     commit message (default: "Update <filename> [skip ci]")
  -u, --base-url <url>        GitLab API base URL (default: "https://gitlab.com/api/v4")
  --fail-on-error             fail the job on error
  -d, --debug                 debug (verbose) mode
  -V, --version               output the version number
  -h, --help                  display help for command
```

## Contributing

All contributions are welcome! Please follow the [code of conduct](https://www.contributor-covenant.org/version/2/0/code_of_conduct/) when interacting with others. [This project lives on GitLab](https://gitlab.com/uninen/push-to-repo).

[Follow @Uninen](https://twitter.com/uninen) on Twitter.
